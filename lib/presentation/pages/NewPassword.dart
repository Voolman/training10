import 'package:flutter/material.dart';
import '../../domain/NewPasswordPresenter.dart';
import '../widgets/CustomTextField.dart';
import '../widgets/dialogs.dart';
import 'LogIn.dart';

class NewPassword extends StatefulWidget {
  const NewPassword({super.key});


  @override
  State<NewPassword> createState() => _NewPasswordState();
}
bool isChecked = false;
class _NewPasswordState extends State<NewPassword> {
  @override
  Widget build(BuildContext context) {
    TextEditingController confirmPassword = TextEditingController();
    TextEditingController password = TextEditingController();
    void onChanged(_){}
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: const EdgeInsets.only(top: 155, left: 24, right: 24),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                children: [
                  Text(
                    'New Password',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ],
              ),
              const SizedBox(height: 8),
              Row(
                children: [
                  Text(
                    'Enter new password',
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                ],
              ),
              const SizedBox(height: 46),
              CustomTextField(
                  label: 'Password',
                  hint: '**********',
                  enableObscure: true,
                  onChanged: onChanged,
                  controller: password
              ),
              CustomTextField(
                  label: 'Confirm Password',
                  hint: '**********',
                  enableObscure: true,
                  onChanged: onChanged,
                  controller: confirmPassword
              ),
              const SizedBox(height: 71),
              SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                      onPressed: (){
                        showLoading(context);
                        pressChangePassword(
                            password.text,
                                (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const LogIn()));},
                                (String e){showError(context, e);}

                        );
                        Navigator.of(context).pop();
                      },
                      style: Theme.of(context).filledButtonTheme.style,
                      child: Text(
                        'Log in',
                        style: Theme.of(context).textTheme.labelMedium,
                      )
                  )
              ),
            ],
          ),
        ),
      ),
    );
  }
}