import 'package:flutter/material.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training_final_6/presentation/pages/SignUp.dart';
import 'package:training_final_6/presentation/theme/colors.dart';
import 'package:training_final_6/presentation/theme/theme.dart';

Future<void> main() async {
  await Supabase.initialize(
    url: 'https://ndpirkfhcctoltoveofs.supabase.co',
    anonKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Im5kcGlya2ZoY2N0b2x0b3Zlb2ZzIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTM3MTYzMDcsImV4cCI6MjAyOTI5MjMwN30.AUff60YmRHhuygJXd0vUJGWpwQafI-_2JT0pGYM50jI',
  );

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({super.key});

  bool isLight = true;

  void changeColorsApp(BuildContext context){
    isLight = !isLight;
    context.findAncestorStateOfType<_MyAppState>()!.onChangedTheme();
  }

  static MyApp of(BuildContext context){
    return context.findAncestorWidgetOfExactType<MyApp>()!;
  }

  ColorsApp getColors(BuildContext context){
    return (isLight) ? lightColors : darkColors;
  }

  ThemeData getCurrentTheme(){
    return (isLight) ? lightTheme : darkTheme;
  }



  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  void onChangedTheme(){
    setState(() {

    });
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: widget.getCurrentTheme(),
      home: const SignUp(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});


  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold();
  }
}

