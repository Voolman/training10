import 'package:supabase_flutter/supabase_flutter.dart';

var supabase = Supabase.instance.client;

Future<AuthResponse> signUp(String email, String password) async {
  return supabase.auth.signUp(email: email, password: password);
}

Future<void> dataSignUp(String userId, String phone, String fullName) async {
  return supabase.from('profiles').insert({
    'id_user': userId,
    'fullname': fullName,
    'phone': phone
  });
}


Future<AuthResponse> signIn(String email, String password) async {
  return supabase.auth.signInWithPassword(email: email, password: password);
}

Future<void> signOut() async {
  return supabase.auth.signOut();
}

Future<void> sendOTP(String email) async {
  return supabase.auth.resetPasswordForEmail(email);
}

Future<AuthResponse> verifyOTP(String email, String code) {
  return supabase.auth.verifyOTP(
      email: email, token: code, type: OtpType.email);
}

Future<UserResponse> changePassword(String password) {
  return supabase.auth.updateUser(UserAttributes(password: password));
}